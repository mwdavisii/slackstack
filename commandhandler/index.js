'use strict';

const CommandHandler    = require("./lib/commandhandler")

exports.handler = (event, context, callback) => {
    CommandHandler.processCommand(event, (err, data) => {
        if(err){
            context.succeed(err.body);
        }else{
            context.succeed(data);
        }
    });
};


