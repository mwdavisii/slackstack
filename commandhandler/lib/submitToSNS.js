const AWS                   = require('aws-sdk');
var lambdaResponse={};
module.exports={
    submitToSNS : function(event, payload, callback, optionalReply){
        var sns = new AWS.SNS();
        var endpointArn = process.env['SNS_ARN'];
        payload.default = JSON.stringify(payload);
        console.log('sending push');
        console.log(JSON.stringify(payload));
        sns.publish({
            Message: JSON.stringify(payload),
            MessageStructure: 'json',
            Subject: payload.command,
            TargetArn: endpointArn
        }, function(err, data) {
            if (err) {
                console.log(err.stack);
                raiseError(err.stack, callback);
                return;
            }else{
                console.log("Sent");
                lambdaResponse.statusCode = 200;
                if(optionalReply === null){
                    lambdaResponse.body = JSON.stringify(
                        {
                            "response_type": "in_channel",
                            "text":"I've dispatched your request to the Minions via SNS."
                        }
                    );
                } else {
                    lambdaResponse.body = JSON.stringify(
                        {
                            "response_type": "in_channel",
                            "text":optionalReply
                        }
                    );
                }
                console.log(lambdaResponse);
                callback(null, lambdaResponse);
            };
        });
    }
}