'use strict'

const AWS                   = require('aws-sdk');
const SNSHelper             = require('./submitToSNS');
//response variables
var lambdaResponse    = {};

module.exports = {
    processCommand : function(event, callback) {
        handleCommand(event, callback);
    }
};

var handleCommand = function(event, callback){
    var qs = require('qs')
    var payload = qs.parse(event.body);
    payload.targetEnvironment = event.requestContext.stage.toUpperCase();
    console.log(payload);
    if(payload.payload){
        //this means this command is an answer to a previous question. Let's get the original message and attach it.
        //btw the payload actually comes in as payload. 
        payload = JSON.parse(payload.payload);
        payload.targetEnvironment = event.requestContext.stage.toUpperCase();
        console.log("response detected");
        handleMessagingResponse(event, payload, callback);
    } else {
        //this is an initital command request, forward to SNS.
        SNSHelper.submitToSNS(event, payload, callback, null);
    };
};

var handleMessagingResponse = function(event, payload, callback){
    console.log("loading previous command by requestID")
    var awsDocs = new AWS.DynamoDB.DocumentClient();
    var params = {
        TableName : "remedybot_commandthreads",
        KeyConditionExpression : "#req = :requestID",
        ExpressionAttributeNames: {
                "#req":"request_id"
            },
        ExpressionAttributeValues: {
                ":requestID":payload.callback_id
            }
        };

    awsDocs.query(params, function(err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
        } else {
            console.log(data.Items);
            if(data.Items.length==0){
                lambdaResponse.statusCode = 500;
                lambdaResponse.body = JSON.stringify({
                    "text":"PC LOAD LETTER: A Timeout Occured."
                });
                callback(responseBody, null);
            } else{
                var previousPayload = JSON.parse(data.Items[0].payload);
                console.log(previousPayload);
                if(!previousPayload.Responses){
                    previousPayload.Responses = [];
                };
                previousPayload.Responses.push(payload);
                //acknowledge the message and who clicked the button.
                var message;
                message = "Ok <@" + payload.user.name + ">, " + payload.actions[0].value + " I got it! Working...";
                SNSHelper.submitToSNS(event, previousPayload, callback, message);
                
            };
        };
    });
};


var raiseError = function(error, callback){
    lambdaResponse.statusCode = 500;
    lambdaResponse.body = error;
    callback(responseBody, null);
};


