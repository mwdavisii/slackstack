## We do a ton with chat ops at work, so I added some fun on my own time. 

This code is probably overkill for most as it is designed to work from AWS SNS topics, but the `lib/extras.js` should be easily modifiable.

The basic architecture of our chat ops stack is `slack command -> command handler lambda -> SNS -> function handler lambda -> slack result`

`core.cloudformation.yaml` is the underlying infrastructure for our implementation. 
 It will provision:
- Dynamo table to store command threads
- Encryption key used to store credentials in SMS (This was written before secrets)
- The role and policy required to function.

Each lambda function is accompanied with a cloudformation template that provisions necessary resources. It expects to be deployed by AWS CodePipeline.

At a minumum to make this work as is, you will have to:
- Create AWS CodeBuild and CodeDeploy pipelines and the S3 buckets to support them
- Deploy the core cloud formation template
- Create the SMS encrypted keys with your slack tokens
- Deploy the command handler project to the pipelines
- Register the new API Gateway address and command entries in slack
- Deploy the extras lambda function.


