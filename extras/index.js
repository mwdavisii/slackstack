'use strict';

const SlackExtras    = require("./lib/extras");

exports.handler = (event, context, callback) => {
    SlackExtras.processExtras(event, (err, data) => {
        if(err){
            context.succeed(err);
        }else{
            context.succeed(data);
        }
    });
};


