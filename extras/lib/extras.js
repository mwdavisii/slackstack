'use strict'

const AWS           = require('aws-sdk');
const nodeRequest   = require('request');
//response variables
var lambdaResponse    = {};

module.exports = {
    processExtras : function(event, callback) {
        handleCommand(event, callback);
    }
};

var handleCommand = function(event, callback){
    
    var payload = JSON.parse(event.Records[0].Sns.Message);
    switch(payload.command){
        case "/tellajoke":
            handleTellaJoke(payload, callback);
        break;

        case "/chucknorris":
            handleChuckNorrisCommand(payload, callback);
        break; 

        case "/askremedybot":
            handleAskRemedyBot(payload, callback);
        break;

        case "/flipacoin":
            handleCoinFlipCommand(payload, callback);
        break;

        default:
        //here we assume the command wasn't meant for this lambda and just go about our business
        callback(null, "OK");

    };
};
var getSecrets = (paramName, callback)=>{
    var ssm = new AWS.SSM();
    var params = {
        Name: paramName,
        WithDecryption: true
    };
    ssm.getParameter(params, (err, result) =>
    {
        callback(null, result.Parameter.Value)
    });     
};
var handleTellaJoke = function(payload, callback){
    console.log("handleTellaJoke")
    var url = process.env['JOKE_API_ENDPOINT'];
    
    nodeRequest(url, function (error, response, body) {
        if (!error && response.statusCode < 299) {
            var joke = JSON.parse(body);
            console.log(body);
            if(joke){
                sendSlackResponse(payload.response_url, joke.joke, callback);
            };
        }else{
            createBotErrorResponse(payload.response_url, "Remote Status Code: " + response.statusCode, callback);
        };
    });
};

var handleCoinFlipCommand=function(payload, callback){
    var result = (Math.floor(Math.random() * 2) == 0) ? 'It came back heads' : 'It came back tails';
    sendSlackResponse(payload.response_url, result, callback);
};


var handleChuckNorrisCommand = function(payload, callback){
    console.log("handleChuckNorrisCommand")
    var url = process.env['CHUCKNORRIS_API_ENDPOINT'];
    
    nodeRequest(url, function (error, response, body) {
        if (!error && response.statusCode < 299) {
            var joke = JSON.parse(body);
            if(joke){
                var parsedJoke = joke.value.joke.replace(/&quot;/g, '\"');
                sendSlackResponse(payload.response_url, parsedJoke, callback);
            };
        }else{
            createBotErrorResponse(payload.response_url, "Remote Status Code: " + response.statusCode, callback);
        };
    });
};

var handleAskRemedyBot = function(payload, callback){
    if(payload.text.toLowerCase().indexOf("who")>-1){
        sendSlackResponse(payload.response_url, "I'm guessing <@abrylov> or maybe <@dbaker>...", callback);
    }else{
        var messages = new Array();
        messages[0] = "No";
        messages[1] = "Not today";
        messages[2] = "It is decidedly so";
        messages[3] = "Without a doubt";
        messages[4] = "Yes definitely";
        messages[5] = "You may rely on it";
        messages[6] = "As I see it yes";
        messages[7] = "Most likely";
        messages[8] = "Outlook good";
        messages[10] = "Signs point to yes";
        messages[11] = "Reply hazy try again";
        messages[12] = "Ask again later";
        messages[13] = "Better not tell you now";
        messages[14] = "Cannot predict now";
        messages[15] = "Concentrate and ask again";
        messages[16] = "Don't count on it";
        messages[17] = "My reply is no";
        messages[18] = "My sources say no";
        messages[19] = "Outlook not so good";
        messages[20] = "Very doubtful";
        var randomnumber = Math.floor(Math.random() * 20);
        sendSlackResponse(payload.response_url, "My Magic :8ball: said, *" + messages[randomnumber] + "*", callback);
    };
};

var sendSlackResponse = function(responseUrl, message, callback){
    var payload = {
        "text":message,
        "as_user":true,
        "response_type":"in_channel",
    };
    var options = {
        "method": "POST",
        "url": responseUrl,
        "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache"
        },
        body: JSON.stringify(payload)
            
    };
    nodeRequest(options, function (error, response, body) {
        console.log(responseUrl);
        if (!error && response.statusCode < 299) {        
            callback(null, "OK");
        }else{
            console.log(error);
            createBotErrorResponse(responseUrl, error + " Status Code:" + response.statusCode, callback);
        };
    });
};

var createBotErrorResponse = function(responseUrl, errorMessage, callback){
    var message ={
        "response_type":"ephemeral",
        "text" : "I'm sorry, Hal. I can't do that.: " + errorMessage
    };
    var options = {
        "method": "POST",
        "url": responseUrl,
        "headers": {
            "content-type": "application/json",
            "cache-control": "no-cache"
        },
        body: JSON.stringify(message)
        
    };
    nodeRequest(options, function (error, response, body) {
        if (!error && response.statusCode < 299) {    
            callback(null, "OK"); 
        };
    });        
};